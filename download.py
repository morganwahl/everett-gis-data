import requests
from urllib.parse import urlencode
import json
import sys

# https://services2.arcgis.com/xJ0MjVdyImL1ajyn/arcgis/rest/services/Buildings/FeatureServer
# https://services2.arcgis.com/xJ0MjVdyImL1ajyn/arcgis/rest/services/Road_lines_from_planimetrics/FeatureServer

BASE_URL = (
    'https://'
    'services2.arcgis.com'
    '/xJ0MjVdyImL1ajyn/arcgis/rest/services/'
)
SERVICE_NAMES = (
    'Buildings',  # Seems to just be MassGIS data
    'Road_lines_from_planimetrics',
    'Adresses',
    'Parcels',
)
EVERYTHING_QUERIES = {
    ('Road_lines_from_planimetrics', 0): 'LENGTH>0',
    ('Buildings', 0): 'FID>-1',
}


def download_service(service_name):
    service_url = BASE_URL + service_name + '/FeatureServer'
    url = service_url + '?' + urlencode({'f': 'json'})
    response = requests.get(url)
    response.raise_for_status()
    metadata = response.json()

    with open(service_name + '-metadata.json', 'w') as out:
        json.dump(metadata, out)

    for layer in metadata['layers']:
        try:
            query = EVERYTHING_QUERIES[(service_name, layer['id'])]
        except KeyError:
            query = 'FID>-1'
        url = service_url + '/query' + '?' + urlencode({
            'f': 'json',
            'geometryType': 'esriGeometryEnvelope',
            'returnGeometry': 'true',
            'layerDefs': '{}:{}'.format(layer['id'], query),
            'outSR': '4326',
        })
        response = requests.get(url)
        response.raise_for_status()
        data = response.json()

        if 'layers' not in data:
            print(data)
            sys.exit(1)

        layer_data = data['layers'][0]
        layer_filename = (
            service_name + '-layer-{id}-{name}.json'.format(**layer)
        )
        with open(layer_filename, 'w') as out:
            json.dump(layer_data, out)


def main():
    for name in SERVICE_NAMES:
        download_service(name)


if __name__ == '__main__':
    main()
